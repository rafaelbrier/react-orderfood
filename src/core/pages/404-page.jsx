import React, { Component } from 'react';
import { Button } from 'react-bootstrap';


export default class PageNotFound extends Component {
    constructor(props) {
        super(props);

        this.goBack = this.goBack.bind(this);
    }

    goBack() {
        this.props.history.goBack();
    }

    render() {
        return (
            <div >
                <section className="content">
                    <div className="error-page">
                        <h2 className="headline text-yellow"> 404</h2>

                        <div className="error-content">
                            <h3>
                                <i className="fas fa-exclamation-triangle" /> Oops! Página não encontrada.</h3>
                        </div>
                        <Button bsStyle="primary" onClick={this.goBack}>Voltar</Button>
                    </div>
                </section>
            </div>
        );
    }
}