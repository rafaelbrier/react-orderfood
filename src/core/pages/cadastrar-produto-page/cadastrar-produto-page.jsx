import React, { Component } from 'react';
import MyCustomBox from '../../components/my-custom-box';
import CadastrarProdutoForm from './cadastrar-produto-form';
import CadastrarProdutoList from './cadastrar-produto-list';
import { connect } from 'react-redux';

import { retrieveTipoProdutoAction, retrieveProdutoAction } from '../../actions/retrieve-data-action';
import { saveProdutoAction } from '../../actions/submit-form-action';

declare var Parse: any;

class CadastrarProdutoPage extends Component {

    componentWillMount() {
        this.props.retrieveTipoProdutoAction("cadastrarProdutoForm");
        this.props.retrieveProdutoAction("listaProduto");
    }

    formSubmit = (values) => {
         return this.props.saveProdutoAction(values, "cadastrarProdutoForm");
    }

    render() {
        return (
            <div >
                <MyCustomBox
                    header="Cadastrar Produto"
                    help={{
                        title: "Título Ajuda",
                        body: "Um corpo de ajuda qualquer"
                    }}
                    loadingBarScope="cadastrarProdutoForm"
                >
                    <CadastrarProdutoForm onSubmit={this.formSubmit}
                        selectParams={this.props.retrieve.tiposProduto.map((m) => { return { "id": m.id, "nome": m.nome } })} />

                </MyCustomBox>

                <MyCustomBox header="Lista de Produtos"
                             loadingBarScope="listaProduto"
                >

                    <CadastrarProdutoList produtoList={this.props.retrieve.produtos} produtoCount={this.props.retrieve.produtoCount}/>

                </MyCustomBox>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        submit: state.submitFormReducer,
        retrieve: state.retrieveDataReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        retrieveTipoProdutoAction: (loadingBarScope) => dispatch(retrieveTipoProdutoAction(loadingBarScope)),
        retrieveProdutoAction: (loadingBarScope) => dispatch(retrieveProdutoAction(loadingBarScope)),
        saveProdutoAction: (values, loadingBarScope) => dispatch(saveProdutoAction(values, loadingBarScope))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CadastrarProdutoPage);