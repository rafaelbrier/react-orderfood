import React, { Component } from 'react';
import '../pages.css';
import MyCustomList from '../../components/my-custom-list';

import { connect } from 'react-redux';
import { retrieveProdutoAction } from '../../actions/retrieve-data-action';


class CadastrarProdutoList extends Component {

    pageChangeEvent = (pageNumber) => {
        if(this.props.produtoCount > 1 && this.props.retrieve.hasConnection) 
        this.props.retrieveProdutoAction('listaProduto', pageNumber - 1)
    }

    render() {    
        return (
            <MyCustomList
                pageHasChanged={this.pageChangeEvent}
                itemCount={this.props.produtoCount}>

                <div className="table-responsive my-list-style">
                    <table className="table table-bordered">
                        <thead>
                            <tr style={{ backgroundColor: "#cfeff9" }}>
                                <th style={{ width: "10%" }}>Código</th>
                                <th style={{ width: "20%" }}>Nome</th>
                                <th style={{ width: "25%" }}>Descrição</th>
                                <th style={{ width: "20%" }}>Tipo</th>
                                <th style={{ width: "10%" }}>Preço</th>
                                <th style={{ width: "5%" }}>Imagem</th>
                                <th style={{ width: "10%" }}>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.produtoList
                                ? this.props.produtoList.map((produto) => (
                                    <tr key={produto._objCount}>
                                        <th >{produto.codigo}</th>
                                        <td>{produto.nome}</td>
                                        <td>{produto.descricao}</td>
                                        <td>{produto.tipo.nome}</td>
                                        <td>{produto.preco}</td>
                                        <td>{produto.tipo.imagem}</td>
                                        <td>Ações</td>
                                    </tr>
                                ))
                                : ' '}
                        </tbody>
                    </table>
                </div>
            </MyCustomList>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        retrieve: state.retrieveDataReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        retrieveProdutoAction: (loadingBarScope, page) => dispatch(retrieveProdutoAction(loadingBarScope, page))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CadastrarProdutoList);