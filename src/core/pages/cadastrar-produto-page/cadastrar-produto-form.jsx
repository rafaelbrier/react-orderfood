import React from 'react';
import '../pages.css';
import { Field, reduxForm, reset } from 'redux-form';
import { Button } from 'react-bootstrap';
import { Validators } from '../../shared/validators';
import { inputField } from '../../components/input-field';

const onSubmitSuccess = (result, dispatch) => {
    dispatch(reset('cadastrarProdutoForm'));
}

const CadastrarProdutoForm = (props) => {

    const { reset, pristine, submitting, selectParams } = props;

    return (
        <form onSubmit={props.handleSubmit}>

            <div className="row">
                <div className="col-md-3 col-sm-3">
                    <Field name="codigo"
                        type="text"
                        label="Código"
                        placeholder="Formato: PDYYYYXXXX"
                        component={inputField}
                        noValidate
                        helpBlock="O código é gerado automaticamente se o campo estiver vazio."
                    />                  
                </div>
            </div>

            <div className="div-inline">
                <div className="input-container">
                    <Field name="nome"
                        type="text"
                        label="Nome *"
                        validate={[Validators.required, Validators.minLength2]}
                        placeholder="Nome do Produto"
                        component={inputField} />
                </div>

                <div className="input-container">
                    <Field name="preco"
                        type="number"
                        label="Preço *"
                        validate={[Validators.required, Validators.number]}
                        component={inputField} />
                </div>
            </div>

            <div className="row">
                <div className="col-md-6 col-sm-6">
                    <Field name="descricao"
                        type="textarea"
                        placeholder="Descrição do produto."
                        label="Descrição *"
                        validate={[Validators.required, Validators.maxLength200, Validators.minLength5]}
                        component={inputField}
                    >
                    </Field>
                </div>
            </div>

            <div className="div-inline">
                <div className="input-container">
                    <Field name="tipo"
                        type="select"
                        label="Tipo *"
                        validate={[Validators.required]}
                        component={inputField}
                        selectParams={selectParams} //[{nome: "nome1", codigo: "codigo1"}, ...]
                    >
                    </Field>
                </div>

                <div className="input-container">
                    <Field name="imagem"
                        type="file"
                        label="Imagem *"
                        validate={[Validators.required, Validators.fileInputValidator, Validators.image]}
                        component={inputField} >
                    </Field>
                </div>
            </div>


            <Button bsStyle="primary" type="button" disabled={pristine || submitting} onClick={reset}
                className="pull-right form-buttom" >Limpar</Button>
            <Button bsStyle="success" type="submit" disabled={pristine || submitting}
                className="pull-right form-buttom" >{submitting ? 'Cadastrando...' : 'Cadastrar'}</Button>
        </form>
    );
}

export default reduxForm({
    form: 'cadastrarProdutoForm',
    onSubmitSuccess: onSubmitSuccess,
})(CadastrarProdutoForm)

