import React, { Component } from 'react';

class HomePage extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">HOME-PAGE</h1>
        </header>        
      </div>
    );
  }
}

export default HomePage;
