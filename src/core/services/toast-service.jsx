import React from 'react';
import ToastHandler from '../shared/toast-constants';
import { toast } from 'react-toastify';

const toastContainer = (toastFields) => {
    return (
        <div>
            <strong>
                {toastFields.title}
            </strong>
            <br />
            <i>{toastFields.body}</i>
        </div>
    );
}

const ToastTrigger = (e) => {
    let toastFields = ToastHandler(e.code);

    if (e.message && e.message.message) {
        toastFields = {
            type: 'error',
            title: `Erro servidor! (${e.message.code})`,
            body: e.message.message
        }
    }

    toast[toastFields.type](toastContainer(toastFields), {
        position: "top-right",
        autoClose: 7000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
    });
}

export default ToastTrigger;