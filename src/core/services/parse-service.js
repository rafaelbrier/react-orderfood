declare var Parse: any;
const RESERVED_WORDS = ['save', 'loadFromParseObject', 'screatedAt', 'updatedAt', 'destroy', 'signUp'];
const environment = {
    production: false,
    version: 'v1.0.0-dev',
    parseUrl: 'http://localhost:1337/parse'
  };

export class ParseService {

    static customParse: any;

    static getParse() {
        if (!this.customParse) {
            Parse.initialize("ffparseid", "CsCaNlN0zJ");
            Parse.serverURL = environment.parseUrl;
            this.customParse = Parse;
        }
        return this.customParse;
    }

    static getObjectExposedAttrs(parseObject) {
        if (parseObject) {
            let parseObjectProperties = Object.keys(parseObject.attributes);
            for (let property of parseObjectProperties) {
                if (property !== 'createdAt' && property !== 'updatedAt') {
                    parseObject[property] = parseObject.get(property);

                }
            }
        }
        return parseObject;
    }

    static updateExposedAttrs(objeto, className) {
        let parseObjetoClass = this.getParse().Object.extend(className);
        let parseObjeto = new parseObjetoClass()
        var properties = Object.keys(objeto);
        for (let property of properties) {
            if (property.charAt(0) !== '_' && RESERVED_WORDS.indexOf(property) === -1) {
                parseObjeto.set(property, objeto[property]);
            }
        }
        return parseObjeto
    }
}