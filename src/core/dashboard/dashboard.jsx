import React, { Component } from 'react';
import { connect } from 'react-redux';

import { checkConnectionAction } from '../actions/retrieve-data-action';
import Routes from '../../main/routes';

import Header from '../template/header';
import SideBar from '../template/sideBar';
import Footer from '../template/footer';

import { BrowserRouter, Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';

class Dashboard extends Component {

  componentWillMount() {
    this.props.checkConnectionAction();
  }

  render() {
    return (
      <BrowserRouter>
        <div className="wrapper">
        <Header />
        <SideBar />
        <div className='content-wrapper'>
          {/* <div className="position: relative">
          <i className="connect-icon fas fa-wifi"
            style={this.props.retrieve.hasConnection ? {color: "green"} : {color: "red"}}
            ></i>
      </div>

      DASHBOARD
      <Link to="/cadastrar-produto">
          <Button bsStyle="success">Cadastrar Produto</Button>
      </Link> */}
          <Routes />
        </div>
        <Footer />
                </div>   

      </BrowserRouter >
    );
  }
}

const mapStateToProps = (state) => {
  return {
    retrieve: state.retrieveDataReducer
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    checkConnectionAction: () => { dispatch(checkConnectionAction()) }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
