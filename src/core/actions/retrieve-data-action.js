import { ActionTypes } from './actionsTypes';
import { collections, listLimit } from '../shared/db-consts';
import { ParseService } from '../services/parse-service';
import ToastTrigger from '../services/toast-service';
import { hideLoading, showLoading } from 'react-redux-loading-bar';

declare var Parse: any;

const checkConnection = "checkConnection";

const getTotalCount = (className) => {
    let queryProdutoCount = new Parse.Query(className)
    return queryProdutoCount.count()
        .then(resCount => {
            return resCount;
        }).catch(e => {

        })
}

export const retrieveTipoProdutoAction = (scope) => {

    return dispatch => {
        dispatch(showLoading(scope));
        let queryTipoProduto = new Parse.Query(collections.TipoProduto);
        return queryTipoProduto.find()
            .then(res => {
                let responseMap = res.map(r => ParseService.getObjectExposedAttrs(r));
                dispatch({
                    type: ActionTypes.RETRIEVE_TIPO_PRODUTO,
                    payload: responseMap
                });
                dispatch(hideLoading(scope));
            })
            .catch(e => {
                dispatch(hideLoading(scope));
                ToastTrigger(e);
            });
    }
}

export const retrieveProdutoAction = (scope, page = 0, limit = listLimit) => {
    return dispatch => {
        dispatch(showLoading(scope));
        let queryProduto = new Parse.Query(collections.Produto);
        queryProduto.limit(limit);
        queryProduto.skip(page * limit);

        return queryProduto.find()
            .then(res => {
                getTotalCount(collections.Produto)
                    .then(resCount => {
                        let responseMap = res.map(r => {
                            r.tipo = ParseService.getObjectExposedAttrs(r.get('tipo'));
                            return ParseService.getObjectExposedAttrs(r);
                        });
                        dispatch({
                            type: ActionTypes.RETRIEVE_PRODUTO,
                            payload: responseMap,
                            produtoCount: resCount
                        });
                        dispatch(hideLoading(scope));
                    })

            })
            .catch(e => {
                dispatch(hideLoading(scope));
                ToastTrigger(e);
            });
    }
}


export const checkConnectionAction = () => {
    return dispatch => {
        Parse.Cloud.run(checkConnection, {})
            .then((response) => {
                dispatch({
                    type: ActionTypes.CHECK_CONNECTION,
                    payload: true,
                });
            })
            .catch(e => {
                dispatch({
                    type: ActionTypes.CHECK_CONNECTION,
                    payload: false,
                });
            })
    }
}

