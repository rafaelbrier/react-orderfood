import { ActionTypes } from './actionsTypes';
import ToastTrigger from '../services/toast-service';
import { ParseService } from '../services/parse-service';
import { collections } from '../shared/db-consts';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
// import { reset } from 'redux-form';

declare var Parse: any;

export const saveProdutoAction = (values, scope) => {

    return dispatch => {
        dispatch(showLoading(scope));
        let produto = ParseService.updateExposedAttrs(values, collections.Produto);
        let TipoProdutoClass = ParseService.getParse().Object.extend(collections.TipoProduto);
        let tipoProduto = new TipoProdutoClass();
        tipoProduto.set('id', values.tipo);

        produto.set('tipo', tipoProduto);
        return produto.save()
            .then(res => {
                dispatch({
                    type: ActionTypes.SAVE,
                    payload: values
                });
                ToastTrigger({code: "success"});
                dispatch(hideLoading(scope));
            })
            .catch(e => {
                dispatch(hideLoading(scope));
                ToastTrigger(e);
            });
    }
}