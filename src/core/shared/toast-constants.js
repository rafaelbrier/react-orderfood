const ERROR_CODES = {
    ConnectionFailed: 100,
    RequestTimeout: 124,
}

//Error Type: success, error, default, warning, info


const ToastHandler = (code) => {
    switch (code) {
        case ("success"):
            return {
                type: 'success',
                title: 'Operação realizada com sucesso!',
                body: ''
            };
        case (ERROR_CODES.ConnectionFailed):
            return {
                type: 'error',
                title: 'Ops, Error!',
                body: "Não foi possível conectar com o servidor! Recarregue a página para tentar novamente!"
            };
        case (ERROR_CODES.RequestTimeout):
            return {
                type: 'error',
                title: 'Ops, Error!',
                body: "O servidor demorou muito para responder!"
            };
        default:
            return {
                type: 'error',
                title: 'Ops, Error!',
                body: "Ocorreu um erro ao realizar a operação, por favor contate o administrador do sistema."
            };
    }
}

export default ToastHandler;