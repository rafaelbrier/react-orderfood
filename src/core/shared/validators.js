const maxLength = max => value => {
    return (value && value.length > max
        ? `O valor deve ter ${max} caracteres ou menos.`
        : undefined);
};
const minLength = min => value => {
    return (value && value.length < min
        ? `O valor deve ter ${min} caracteres ou mais.`
        : undefined);
};
const required = value => {
    return (value || typeof value === "number"
        ? undefined
        : "Campo Obrigatório!");
};
const fileInputValidator = value => {
    return (value && value.length === 0 
        ? "Selecione uma imagem!" 
        : undefined)
};
const image = value => {
    if(value && value[0])
    { 
        return  (["image/gif", "image/x-icon", "image/jpeg", "image/png", "image/svg+xml", "image/tiff", "image/webp"]
        .includes(value[0].type)
        ? undefined
        : "O arquivo deve ser uma imagem!");

     }  else if (value) {
         return (["image/gif", "image/x-icon", "image/jpeg", "image/png", "image/svg+xml", "image/tiff", "image/webp"]
         .includes(value.type)
         ? undefined
        : "O arquivo deve ser uma imagem!");
     } else { return undefined }    
}
const email = value => {
    return (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? "Email inválido."
        : undefined);
};
const number = value => {
    return (value && isNaN(Number(value)) 
        ? "Deve ser um número."
        : undefined);
};
const phoneNumber = value => {
    return (value && !/^[(]\d{2}[)]\d{4,5}[-]\d{4}$/i.test(value)
        ? "Número de telefone inválido."
        : undefined);
};

export const Validators = {
    required: required,
    email: email,
    number: number,
    image: image,
    fileInputValidator: fileInputValidator,
    phoneNumber: phoneNumber,
    minLength2: minLength(2),
    minLength5: minLength(5),
    maxLength200: maxLength(200),
}

// const minValue = min => value => {
//     return (value && value < min
//         ? `Must be at least ${min}.`
//         : undefined);
// };
// const alphaNumeric = value => {
//     return (value && /[^a-zA-Z0-9 ]/i.test(value)
//         ? "Only alphanumeric characters."
//         : undefined);
// };






