import React from 'react';
import './components.css';

export const inputField = ({
    input,
    helpBlock,
    label,
    type,
    placeholder,
    noValidate,
    inputStyles,
    selectParams,
    meta: { touched, error, warning }
}) => {
    if (type === "select") {
        return (
            <div className="form-group">
                <label className={noValidate ? '' :
                    (touched && error ? 'has-error' : '')}
                >{label}
                </label>
                <select {...input} className={noValidate ? "form-control" : "form-control " +
                    (touched && error ? 'input-has-error ' : '')}
                    placeholder={placeholder} type={type} style={inputStyles}>
                    <option value="">Selecione...</option>
                    {selectParams ? selectParams.map(options => (
                        <option value={options.id} key={options.id}>
                            {options.nome}
                        </option>
                    )) : ''}
                </select>
                {touched &&
                    ((error && <span className="has-error">{error}</span>) ||
                        (warning && <span className="has-warning">{warning}</span>))}
                <small className="help-block">{helpBlock}</small>
            </div>
        );

    } else if (type === "file") {

        const handleChange = (e) => {
            input.onChange(e.target.files[0]);
        }

        return (<div className="form-group" >
            <label className={noValidate ? '' :
                (touched && error ? 'has-error'
                    : (touched && !error && !warning ? 'success' : ''))}
            >{label}
            </label>
            <div style={{ width: "105%" }}>
                <input {...input} className={noValidate ? "form-control" : "form-control " +
                    (touched && error ? 'input-has-error ' : '') +
                    (touched && warning ? 'input-has-warning ' : '') +
                    (touched && !error && !warning ? 'input-success' : '')}
                    type={type} style={inputStyles} value={undefined}
                    onChange={handleChange} />
                {touched &&
                    ((error && <span className="has-error">{error}</span>) ||
                        (warning && <span className="has-warning">{warning}</span>))}
                <small className="help-block">{helpBlock}</small>

            </div>
        </div>
        );

    } else {
        return (
            <div className="form-group" >
                <label className={noValidate ? '' :
                    (touched && error ? 'has-error'
                        : (touched && !error && !warning ? 'success' : ''))}
                >{label}
                </label>
                <div>
                    <input {...input} className={noValidate ? "form-control" : "form-control " +
                        (touched && error ? 'input-has-error ' : '') +
                        (touched && warning ? 'input-has-warning ' : '') +
                        (touched && !error && !warning ? 'input-success' : '')}
                        placeholder={placeholder} type={type} style={inputStyles} />
                    {touched &&
                        ((error && <span className="has-error">{error}</span>) ||
                            (warning && <span className="has-warning">{warning}</span>))}
                    <small className="help-block">{helpBlock}</small>

                </div>
            </div>
        );
    }
}
