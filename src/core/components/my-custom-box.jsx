import React, { Component } from 'react';
import './components.css';

import { Panel, Tooltip, OverlayTrigger } from 'react-bootstrap';
import  LoadingBarComp  from '../components/loading-bar-component';

//Inputs: body, help.title, help.body 

export default class MyCustomBox extends Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = { showHide: true };
    }

    toggle() {
        this.setState({ showHide: !this.state.showHide });
    }

    render() {
        return (
            <div className="Container my-custom-shadow" >
                <Panel bsStyle="info"
                    expanded={this.state.showHide}
                    onToggle={this.toggle}>

                    <Panel.Heading>

                        <Panel.Title componentClass="h3">{this.props.header}
                            
                            {this.props.help ?
                                (<span className="bloco-ajuda">
                                    <OverlayTrigger placement="right" overlay={
                                        <Tooltip id="tooltip">
                                            <strong>{this.props.help.title}</strong>
                                            <br />
                                            <i>{this.props.help.body}</i>
                                        </Tooltip>
                                    }>
                                        <i className="fas fa-question" />
                                    </OverlayTrigger>
                                </span>)
                                : null}

                            <Panel.Toggle className="pull-right">
                                {this.state.showHide ? <i className="fas fa-minus"/> : <i className="fas fa-plus" />}
                            </Panel.Toggle>

                        </Panel.Title>
                    </Panel.Heading>
                    <LoadingBarComp loadingBarScope={this.props.loadingBarScope ? this.props.loadingBarScope : undefined}/>
                    
                    <Panel.Collapse>
                        <Panel.Body >{this.props.children}</Panel.Body>
                    </Panel.Collapse>

                </Panel>
            </div >
        );
    }
}
