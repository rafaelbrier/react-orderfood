import React, { Component } from 'react';
import './components.css';
import { Pagination } from 'react-bootstrap';
import { listLimit } from '../shared/db-consts';

export default class MyCustomList extends Component {

    totalPage: Number;

    constructor(props) {
        super(props);

        this.state = {
            currentPage: 1
        }
    }

    onClickPage = (pageNumber) => {
        this.props.pageHasChanged(pageNumber);
        this.setState({ ...this.state, currentPage: pageNumber });
    }

    onClickFirst = () => {
        return (this.state.currentPage === 1) ? null : this.onClickPage(1);
    }

    onClickLast = () => {
        return (this.state.currentPage === this.totalPage || this.totalPage + 1 === 1) ? null : this.onClickPage(this.totalPage);
    }

    onClickPrev = () => {
        return (this.state.currentPage === 1) ? null : this.onClickPage(this.state.currentPage - 1);
    }

    onClickNext = () => {
        return (this.state.currentPage === this.totalPage || this.totalPage + 1 === 1) ? null : this.onClickPage(this.state.currentPage + 1);
    }

    pagination(currentPage: number, pageCount: number) {
        const delta = 3;

        let range = [];

        if (pageCount <= 1) {
            range.push(1);
            return range;
        }

        for (let i = Math.max(2, currentPage - delta); i <= Math.min(pageCount - 1, currentPage + delta); i++) {
            range.push(i);
        }

        if (currentPage - delta > 2) {
            range.unshift("...");
        }
        if (currentPage + delta < pageCount - 1) {
            range.push("...");
        }

        range.unshift(1);
        range.push(pageCount);

        return range;
    }


    listaVaziaComponent() {
        return (
            <div className="table-empty text-center">
                <div>
                    <div className="table-empty-icon">
                        <i className="fa fa-search fa-3x"></i>
                    </div>
                </div>
                <div style={{ opacity: 0.5 }}>
                    <h3>Lista Vazia</h3>
                </div>
            </div>
        );
    }

    render() {
        this.totalPage = Math.ceil(this.props.itemCount / listLimit);
        let EllipsisKey = 0;

        return (
            <div>

                {this.props.itemCount < 1 ? this.listaVaziaComponent() : this.props.children}


                <Pagination className="pull-right">

                    <Pagination.First onClick={() => this.onClickFirst()} />

                    <Pagination.Prev onClick={() => this.onClickPrev()} />

                    {this.pagination(this.state.currentPage, this.totalPage).map((pageNumber) => {

                        if (pageNumber === "...") {
                            return (
                                <Pagination.Ellipsis key={"e" + EllipsisKey++} disabled />
                            );
                        } else {
                            return (
                                <Pagination.Item key={pageNumber}
                                    active={this.state.currentPage === pageNumber}
                                    onClick={() => this.onClickPage(pageNumber)} >{pageNumber}</Pagination.Item>
                            );
                        }
                    })}

                    <Pagination.Next onClick={() => this.onClickNext()} />

                    <Pagination.Last onClick={() => this.onClickLast()} />
                </Pagination>

                <b className="pull-left" style={{ marginTop: 30 }}>Total: {this.props.itemCount} {this.props.itemCount === 1
                    ? ' item'
                    : ' itens'}</b>

            </div>
        );
    }
}