import React, { Component } from 'react';
import LoadingBar from 'react-redux-loading-bar'
 
export default class LoadingBarComp extends Component {
  render() {
    return (
      <header>
        <LoadingBar showFastActions style={{backgroundColor: '#17a3c6'}} scope={this.props.loadingBarScope}/>
      </header>
    )
  }
}