import { ActionTypes } from '../actions/actionsTypes';

const initialState = {
    hasConnection: false,    
    tiposProduto: [],
    produtos: [],
    produtoCount: 0
};

const retrieveDataReducer = (state = initialState, action) => {
    switch (action.type) {

        case ActionTypes.RETRIEVE_TIPO_PRODUTO:
            return {
                ...state,
                tiposProduto: action.payload
            };

        case ActionTypes.RETRIEVE_PRODUTO:
            return {
                ...state,
                produtos: action.payload,
                produtoCount: action.produtoCount
            };

        case ActionTypes.CHECK_CONNECTION:
            return {
                ...state,
                hasConnection: action.payload
            };

        default:
            return state;
    }
}

export default retrieveDataReducer;
