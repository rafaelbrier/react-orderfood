import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'
import  retrieveDataReducer  from './retrieve-data-reducer';
import submitFormReducer from './submit-form-reducer';
import { loadingBarReducer } from 'react-redux-loading-bar'

export default combineReducers({
    form: formReducer,
    retrieveDataReducer,
    submitFormReducer,
    loadingBar: loadingBarReducer,
});