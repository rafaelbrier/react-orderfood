import { ActionTypes } from '../actions/actionsTypes';

const initialState = {
    codigo: null,
    nome: null,
    descricao: null,
    preco: null,
    tipo: null,
    imagem: null,
}

const submitFormReducer = (state = initialState, action) => {
    switch (action.type) {

        case ActionTypes.SAVE:
            return state = {
                ...state,
                ...action.payload 
            };

        default:
            return state;
    }
}

export default submitFormReducer;
