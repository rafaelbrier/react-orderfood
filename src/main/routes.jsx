import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

//-------------------------------------------------------------------------
import PageNotFound404 from '../core/pages/404-page';
import HomePage from '../core/pages/home-page/home-page';
import CadastrarProdutoPage from '../core/pages/cadastrar-produto-page/cadastrar-produto-page';


class Routes extends Component {

  render() {
    return (
        <Switch>
          <Route path="/" exact />
          <Route path="/home" component={HomePage} />
          <Route path="/cadastrar-produto" component={CadastrarProdutoPage} />
          <Route path='*' component={PageNotFound404} />
        </Switch>
    );
  }
}

export default Routes;