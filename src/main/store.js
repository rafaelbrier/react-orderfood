import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import combineReducers from '../core/reducers/index';

import promise from 'redux-promise-middleware';
// import { createLogger  } from 'redux-logger';

const initialState = {};

const middleware = [thunk, promise()];
// createLogger()

const store = createStore(
    combineReducers,
    initialState,
    compose(
    applyMiddleware(...middleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )
);

export default store;