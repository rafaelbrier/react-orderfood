import React, { Component } from 'react';
import './App.css';
import '../core/template/dependencies';
import { ParseService } from '../core/services/parse-service';

import LoginPage from '../core/pages/login-page/login-page';
import Dashboard from '../core/dashboard/dashboard';

//Toast-------------------------------------------------------------------
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

//Initialize Parse Server ------------------------------------------------
ParseService.getParse();

class App extends Component {

  userLoggedIn: boolean;

  constructor(props){
    super(props);

    this.userLoggedIn = true;
  }

  render() {
    return (  
      <div>
         <ToastContainer toastClassName="toast-shadow" />
        {this.userLoggedIn ? <Dashboard /> : <LoginPage />}
      </div>
    );
  }
}

export default App;
